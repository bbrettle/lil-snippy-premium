'''

TO DO
Add hot key menu, transperancy slider, support multi-monitor, config file,

'''
import sys

import time

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication

from PIL import ImageGrab

class App(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        #hotkeys
        self.hot_keys = {"snip" : "F1",
                       "video" : "F2",
                       "sound" : "F3",
                       "auto" : "F4"
        }
        #labels
        self.imageLabel = QtWidgets.QLabel(self)

        #snip attributes
        self.outsideSquareColor = 'lightblue'
        self.outsideSquareTrans = .5
        self.squareBoundaryColor = 'red'
        self.squareThickness = 2

        #timer
        self.sleepTime = 0

        #file attributes
        self.autoSave = False

        #User Interface
        self.initUI()

        #Snipper Connection
        self.snipper = SnippingWidget()
        self.snipper.closed.connect(self.on_closed)

    def initUI(self):
        #Window Settings
        self.setWindowTitle("Lil Snippy")
        self.setWindowIcon(QtGui.QIcon("assets/lilSnippyIcon.png"))
        self.setGeometry(400, 100, 400, 100)
        #Init Label
        self.setCentralWidget(self.imageLabel)
        self.imageLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.pixmap = QtGui.QPixmap('assets\lilSnippyLogo.png')
        self.imageLabel.setPixmap(self.pixmap)

        #Main Menu
        self.mainMenu()

    def mainMenu(self):
        #Menu bar init
        menubar = self.menuBar()

        #File menu
        fileMenu = menubar.addMenu("File")
        saveAct = QtWidgets.QAction(QtGui.QIcon("assets/saveIcon.png"), "Save", self)
        saveAct.triggered.connect(self.saveSnip)
        saveAsAct = QtWidgets.QAction(
            QtGui.QIcon("assets/saveAsIcon.png"), "Save As", self
        )
        saveAsAct.triggered.connect(self.saveAsSnip)
        fileMenu.addAction(saveAct)
        fileMenu.addAction(saveAsAct)

        #Tool menu
        toolMenu = menubar.addMenu("Tools")
        snipAct = QtWidgets.QAction(QtGui.QIcon("assets/cameraIcon.png"), "Snipping Tool", self)
        snipAct.setShortcut(QtGui.QKeySequence(self.hot_keys["snip"]))
        snipAct.triggered.connect(self.activateSnipping)
        videoAct = QtWidgets.QAction(QtGui.QIcon("assets/videoIcon.png"), "Video Capture", self)
        videoAct.setShortcut(QtGui.QKeySequence(self.hot_keys["video"]))
        videoAct.triggered.connect(self.activateVideo)
        soundAct = QtWidgets.QAction(QtGui.QIcon("assets/audioIcon.png"), "Sound Capture", self)
        soundAct.setShortcut(QtGui.QKeySequence(self.hot_keys["sound"]))
        soundAct.triggered.connect(self.activateSound)
        autoAct = QtWidgets.QAction(
            QtGui.QIcon("assets/automationIcon.png"), "Automation Station", self
        )
        autoAct.setShortcut(QtGui.QKeySequence(self.hot_keys["auto"]))
        autoAct.triggered.connect(self.activateAutomation)
        # autoAct.setShortcut("F4")
        toolMenu.addAction(snipAct)
        toolMenu.addAction(videoAct)
        toolMenu.addAction(soundAct)
        toolMenu.addAction(autoAct)

        #Options tab
        optionsMenu = menubar.addMenu("Options")

        #Options tab -> Snip options
        snipMenu = QtWidgets.QMenu('Snip Options',self)
        optionsMenu.addMenu(snipMenu)
        # Options tab -> Snip options -> Colors
        snipColorMenu = QtWidgets.QMenu('Colors',self)
        snipMenu.addMenu(snipColorMenu)
        # Options tab -> Snip options -> Colors ->
        outsideSnipColorAct = QtWidgets.QAction(QtGui.QIcon("assets/outsideColorIcon.png"), "Outside Bounds Color", self)
        snipColorMenu.addAction(outsideSnipColorAct)
        outsideSnipColorAct.triggered.connect(self.setOutsideColor)
        outsideSquareSnipTransAct = QtWidgets.QAction(QtGui.QIcon("assets/transparencyIcon.png"),
                                                      "Outside Bounds Transparency", self)
        snipColorMenu.addAction(outsideSquareSnipTransAct)
        outsideSquareSnipTransAct.triggered.connect(self.setSnipTransparency)
        squareSnipColorAct = QtWidgets.QAction(QtGui.QIcon("assets/squareColorIcon.png"), "Snip Square Color", self)
        snipColorMenu.addAction(squareSnipColorAct)
        squareSnipColorAct.triggered.connect(self.setSquareBoundaryColor)
        # Options tab -> Snip options -> Configuration
        snipConfigurationAct = QtWidgets.QAction(QtGui.QIcon("assets/optionsIcon.png"), "Snip Configuration", self)
        snipMenu.addAction(snipConfigurationAct)
        snipConfigurationAct.triggered.connect(self.snipConfigOptions)
        # Options tab -> Snip options -> sleep timer
        sleepConfigurationAct = QtWidgets.QAction(QtGui.QIcon("assets/timeIcon.png"), "Sleep Timer", self)
        snipMenu.addAction(sleepConfigurationAct)
        sleepConfigurationAct.triggered.connect(self.sleepTimer)

        # Options tab -> Sound options
        soundMenu = QtWidgets.QMenu('Sound Options', self)
        optionsMenu.addMenu(soundMenu)
        # Options tab -> Sound options -> Hardware
        hardwareConfigurationAct = QtWidgets.QAction(QtGui.QIcon("assets/audioMicIcon.png"), "Hardware", self)
        soundMenu.addAction(hardwareConfigurationAct)
        # Options tab -> Sound options -> Audio configuration
        soundConfigurationAct = QtWidgets.QAction(QtGui.QIcon("assets/cdIcon.png"), "Audio Configuration", self)
        soundMenu.addAction(soundConfigurationAct)

        #Help tab
        helpMenu = menubar.addMenu("Help")
        helpAct = QtWidgets.QAction(QtGui.QIcon("assets/helpIcon.png"), "Help", self)
        aboutAct = QtWidgets.QAction(QtGui.QIcon("assets/aboutIcon.png"), "About", self)
        helpMenu.addAction(helpAct)
        helpMenu.addAction(aboutAct)

    def activateSnipping(self):
        print("Snipping activated")
        self.hide()
        time.sleep(self.sleepTime)
        self.snipper.showFullScreen()
        QApplication.setOverrideCursor(QtCore.Qt.CrossCursor)

    def activateVideo(self):
        print("Video recording activated")

    def activateSound(self):
        print("Sound recording activated")

    def activateAutomation(self):
        print("Automation activated")

    #Application display methods
    def displayPicture(self):
        self.pixmap = QtGui.QPixmap('lastImage.png')
        # self.img_preview.setPixmap(self.preview_screen.scaled(350, 350,
        #                                                       Qt.KeepAspectRatio, Qt.SmoothTransformation))
        self.imageLabel.setPixmap(self.pixmap)
        # self.imageLabel.setPixmap(self.pixmap.scaled(self.pixmap.height(),
        #                                              self.pixmap.width(), QtCore.Qt.KeepAspectRatio,
        #                                              QtCore.Qt.SmoothTransformation))
        # self.screenshotLabel.setScaledContents(True)# RESIZES to fit space! Good for magnify?
        # self.resize(self.pixmap.width(), self.pixmap.height())
        # self.originalPixmap.save('snips/testImage.png', 'png')

    def resizeEvent(self, event):
        print("Resized")

    def on_closed(self):
        self.displayPicture()
        self.show()

    #Snip Option methods
    def setOutsideColor(self):
        self.outsideSquareColor = QtWidgets.QColorDialog.getColor()

    def setSquareBoundaryColor(self):
        self.squareBoundaryColor = QtWidgets.QColorDialog.getColor()

    def setSnipTransparency(self):
        print("setting transparency")

    def snipConfigOptions(self):
        print("snipConfig")
        self.snipConfig = SnipConfiguration()

    def sleepTimer(self):
        print("sleep")
        self.sleepTime = 5

    #File methods
    def saveSnip(self):
        print("save")

    def saveAsSnip(self):
        format = 'png'
        initialPath = QtCore.QDir.currentPath() + "/untitled." + format

        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save As", initialPath,
                "%s Files (*.%s);;All Files (*)" % (format.upper(), format))
        if fileName:
            try:
                self.pixmap.save(fileName, format)
            except Exception as e:
                print(e)

class SnipConfiguration(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        # TODO add image type, auto image save increment type/default name
        # FIXME look at the tabdialog.py and groupbox.py in the examples to get this right
        # #Auto Save Check Box
        # autoSaveCheckBox = QtWidgets.QCheckBox('Auto Save', self)
        # autoSaveCheckBox.move(20, 20)
        # autoSaveCheckBox.toggle()
        # autoSaveCheckBox.stateChanged.connect(self.autoSave)
        # #Auto Image Show
        # autoImageShowCheckBox = QtWidgets.QCheckBox('Auto Image Show', self)
        # autoImageShowCheckBox.move(20, 60)
        # autoImageShowCheckBox.toggle()
        # autoImageShowCheckBox.stateChanged.connect(self.autoSave)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('QCheckBox')
        self.show()

    def autoSave(self, state):
        if state == QtCore.Qt.Checked:
            self.setWindowTitle('QCheckBox')
        else:
            self.setWindowTitle(' ')

class SnippingWidget(QtWidgets.QMainWindow):
    closed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(SnippingWidget, self).__init__(parent)
        self.setAttribute(QtCore.Qt.WA_NoSystemBackground, True)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        self.setStyleSheet("background:transparent;")
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)

        self.start_point = QtCore.QPoint()
        self.end_point = QtCore.QPoint()

    def mousePressEvent(self, event):
        self.start_point = event.pos()
        self.end_point = event.pos()
        self.update()

    def mouseMoveEvent(self, event):
        self.end_point = event.pos()
        self.update()

    def mouseReleaseEvent(self, QMouseEvent):
        try:
            r = QtCore.QRect(self.start_point, self.end_point).normalized()
            self.hide()
            self.snippedImg = ImageGrab.grab(bbox=r.getCoords()) # got what I needed
            self.snippedImg.save("lastImage.png") #Don't want to have to save it to use this image
            QApplication.restoreOverrideCursor()
            self.start_point = QtCore.QPoint()
            self.end_point = QtCore.QPoint()
            self.closed.emit()
        except Exception as e:
            print(e)
            print("No mouse movement")
            QApplication.restoreOverrideCursor()
            self.closed.emit()

    def paintEvent(self, event):
        trans = QtGui.QColor(Application.outsideSquareColor)
        r = QtCore.QRectF(self.start_point, self.end_point).normalized()
        qp = QtGui.QPainter(self)
        trans.setAlphaF(Application.outsideSquareTrans)
        qp.setBrush(trans)
        outer = QtGui.QPainterPath()
        outer.addRect(QtCore.QRectF(self.rect()))
        inner = QtGui.QPainterPath()
        inner.addRect(r)
        r_path = outer - inner
        qp.drawPath(r_path)
        qp.setPen(QtGui.QPen(QtGui.QColor(Application.squareBoundaryColor), Application.squareThickness))
        trans.setAlphaF(0)
        qp.setBrush(trans)
        qp.drawRect(r)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Application = App()
    Application.show()
    sys.exit(app.exec_())
